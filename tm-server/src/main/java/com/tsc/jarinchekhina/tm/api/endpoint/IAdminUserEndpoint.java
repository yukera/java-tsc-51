package com.tsc.jarinchekhina.tm.api.endpoint;

import com.tsc.jarinchekhina.tm.api.IEndpoint;
import com.tsc.jarinchekhina.tm.dto.UserDTO;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAdminUserEndpoint extends IEndpoint<User> {

    @NotNull
    UserDTO findByLogin(@Nullable SessionDTO session, @Nullable String login);

    @NotNull
    UserDTO lockByLogin(@Nullable SessionDTO session, @Nullable String login);

    void removeByLogin(@Nullable SessionDTO session, @Nullable String login);

    @NotNull
    UserDTO unlockByLogin(@Nullable SessionDTO session, @Nullable String login);

}
