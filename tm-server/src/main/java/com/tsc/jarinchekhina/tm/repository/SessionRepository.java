package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.ISessionRepository;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class SessionRepository implements ISessionRepository {

    @NotNull
    private final EntityManager entityManager;

    public SessionRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void add(@NotNull final SessionDTO session) {
        entityManager.persist(session);
    }

    @Override
    public void addAll(@NotNull Collection<SessionDTO> collection) {
        for (@NotNull SessionDTO session : collection) {
            add(session);
        }
    }

    @Override
    public void update(@NotNull final SessionDTO session) {
        entityManager.merge(session);
    }

    @Override
    @SneakyThrows
    public void clear() {
        entityManager
                .createQuery("DELETE FROM SessionDTO")
                .executeUpdate();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDTO> findAll() {
        return entityManager
                .createQuery("SELECT e FROM SessionDTO e", SessionDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO findById(@NotNull final String id) {
        @Nullable final List<SessionDTO> sessionList = entityManager
                .createQuery("SELECT e FROM SessionDTO e WHERE e.id = :id", SessionDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (sessionList == null || sessionList.isEmpty()) throw new AccessDeniedException();
        return sessionList.get(0);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDTO> findByUserId(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT e FROM SessionDTO e WHERE e.userId = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final SessionDTO session) {
        removeById(session.getId());
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String id) {
        @NotNull final SessionDTO session = entityManager.getReference(SessionDTO.class, id);
        entityManager.remove(session);
    }

}
