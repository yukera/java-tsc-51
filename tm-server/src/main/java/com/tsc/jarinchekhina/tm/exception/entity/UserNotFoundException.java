package com.tsc.jarinchekhina.tm.exception.entity;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public final class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
