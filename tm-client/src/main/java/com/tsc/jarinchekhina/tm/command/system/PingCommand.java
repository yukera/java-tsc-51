package com.tsc.jarinchekhina.tm.command.system;

import com.tsc.jarinchekhina.tm.command.AbstractUserCommand;
import com.tsc.jarinchekhina.tm.endpoint.Session;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class PingCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "system-ping";
    }

    @NotNull
    @Override
    public String description() {
        return "ping server";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[PING]");
        if (serviceLocator.getSessionEndpoint().ping())
            System.out.println("Success");
        else
            System.out.println("No connect");
    }

}
